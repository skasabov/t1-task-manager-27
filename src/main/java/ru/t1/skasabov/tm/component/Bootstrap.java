package ru.t1.skasabov.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.skasabov.tm.api.repository.ICommandRepository;
import ru.t1.skasabov.tm.api.repository.IProjectRepository;
import ru.t1.skasabov.tm.api.repository.ITaskRepository;
import ru.t1.skasabov.tm.api.repository.IUserRepository;
import ru.t1.skasabov.tm.api.service.*;
import ru.t1.skasabov.tm.command.AbstractCommand;
import ru.t1.skasabov.tm.command.data.AbstractDataCommand;
import ru.t1.skasabov.tm.command.data.DataBase64LoadCommand;
import ru.t1.skasabov.tm.command.data.DataBinaryLoadCommand;
import ru.t1.skasabov.tm.enumerated.Role;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.exception.system.ArgumentEmptyException;
import ru.t1.skasabov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.skasabov.tm.exception.system.CommandEmptyException;
import ru.t1.skasabov.tm.exception.system.CommandNotSupportedException;
import ru.t1.skasabov.tm.model.Project;
import ru.t1.skasabov.tm.model.User;
import ru.t1.skasabov.tm.repository.CommandRepository;
import ru.t1.skasabov.tm.repository.ProjectRepository;
import ru.t1.skasabov.tm.repository.TaskRepository;
import ru.t1.skasabov.tm.repository.UserRepository;
import ru.t1.skasabov.tm.service.*;
import ru.t1.skasabov.tm.util.SystemUtil;
import ru.t1.skasabov.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

@Getter
@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull private static final String PACKAGE_COMMANDS = "ru.t1.skasabov.tm.command";

    @NotNull private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull private final IUserRepository userRepository = new UserRepository();

    @NotNull private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull private final IProjectTaskService projectTaskService = new ProjectTaskService(
            projectRepository,
            taskRepository
    );

    @NotNull private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull private final ILoggerService loggerService = new LoggerService();

    @NotNull private final IPropertyService propertyService = new PropertyService();

    @NotNull private final IUserService userService = new UserService(
            userRepository, taskRepository, projectRepository, propertyService
    );

    @NotNull private final IAuthService authService = new AuthService(userService, propertyService);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    private void initData() {
        final boolean checkBinary = Files.exists(Paths.get(AbstractDataCommand.FILE_BINARY));
        if (checkBinary) processCommand(DataBinaryLoadCommand.NAME, false);
        if (checkBinary) return;
        final boolean checkBase64 = Files.exists(Paths.get(AbstractDataCommand.FILE_BASE64));
        if (checkBase64) processCommand(DataBase64LoadCommand.NAME, false);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    public static void close() {
        System.exit(0);
    }

    private void initDemoData() {
        @NotNull final User test = userService.create("test", "test", "test@test.ru");
        userService.create("user", "user", "user@user.ru");
        userService.create("admin", "admin", Role.ADMIN);

        projectService.add(test.getId(), new Project("TEST PROJECT", Status.IN_PROGRESS));
        projectService.add(test.getId(), new Project("DEMO PROJECT", Status.NOT_STARTED));
        projectService.add(test.getId(), new Project("BEST PROJECT", Status.IN_PROGRESS));
        projectService.add(test.getId(), new Project("BETA PROJECT", Status.COMPLETED));

        taskService.create(test.getId(), "MEGA TASK");
        taskService.create(test.getId(), "BETA TASK");
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void start(@Nullable final String[] args) {
        if (processArguments(args)) close();
        processCommands();
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(
                () -> loggerService.info("** TASK MANAGER IS SHUTTING DOWN **"))
        );
    }

    private void processCommands() {
        initPID();
        initDemoData();
        initLogger();
        initData();

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @Nullable final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            }
            catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private boolean processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String argument = args[0];
        try {
            processArgument(argument);
            loggerService.argument(argument);
        }
        catch (@NotNull final Exception e) {
            loggerService.error(e);
        }
        return true;
    }

    private void processCommand(@Nullable final String command) {
        processCommand(command, true);
    }

    private void processCommand(@Nullable final String command, final boolean checkRoles) {
        if (command == null || command.isEmpty()) throw new CommandEmptyException();
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        if (checkRoles) authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void processArgument(@Nullable final String argument) {
        if (argument == null || argument.isEmpty()) throw new ArgumentEmptyException();
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

}

package ru.t1.skasabov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.util.TerminalUtil;

@NoArgsConstructor
public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @NotNull private static final String NAME = "task-update-by-id";

    @NotNull private static final String DESCRIPTION = "Update task by id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        getTaskService().updateById(userId, id, name, description);
    }

}

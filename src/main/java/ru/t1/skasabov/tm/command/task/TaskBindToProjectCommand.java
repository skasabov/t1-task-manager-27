package ru.t1.skasabov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.util.TerminalUtil;

@NoArgsConstructor
public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    @NotNull private static final String NAME = "task-bind-to-project";

    @NotNull private static final String DESCRIPTION = "Bind task to project.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
    }

}
